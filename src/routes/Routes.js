import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import {
    Login,
    Register,
    ToDoTasks,
    DoneTasks,
    App,
    Task,
} from '../screens/Screens';
import React from 'react';

const Stack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();

export const TaskTab = () => {

    return (
        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: '#246BFA',
                inactiveTintColor: 'gray',
                iconStyle: { width: 20, height: 20 },
            }}>
            <Tab.Screen name="Afazeres" component={ToDoTasks} />


            <Tab.Screen name="Realizadas" component={DoneTasks} />
        </Tab.Navigator>
    );
};
const Routes = () => {
    return (
        <Stack.Navigator headerMode="screen">
            <Stack.Screen name="App" component={App} options={{ headerShown: false }} />
            <Stack.Screen
                name="Login"
                component={Login}
                options={{ headerShown: false }}
            />
            <Stack.Screen name="Register" component={Register}  options={{
                    title: 'Cadastro de Usuario',
                    headerStyle: {
                        backgroundColor: '#246BFA',
                    },
                    headerTintColor: '#ffffff',
                    headerTitleStyle: {
                        fontWeight: 'normal',
                    },
                }} />
            
            <Stack.Screen name="TaskList" component={TaskTab}
                options={{
                    title: 'Lista de Tarefas',
                    headerStyle: {
                        backgroundColor: '#246BFA',
                    },
                    headerTintColor: '#ffffff',
                    headerTitleStyle: {
                        fontWeight: 'normal',
                    },
                }} />
            <Stack.Screen name="Task" component={Task}
                options={{
                    title: 'Criar Tarefa',
                    headerStyle: {
                        backgroundColor: '#246BFA',
                    },
                    headerTintColor: '#ffffff',
                    headerTitleStyle: {
                        fontWeight: 'normal',
                    },
                }} />
        </Stack.Navigator>
    );
};
export default Routes;